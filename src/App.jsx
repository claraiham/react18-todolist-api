import { useState, useEffect } from "react";
import axios from "axios";
import "./App.css";
import TodoList from "./components/TodoList";

const API_URL = "https://jsonplaceholder.typicode.com/todos";

function App() {
  const [todos, setTodos] = useState([]); // on part d'un tableau vide qui va être rempli par les données du fichier JSON

  useEffect(() => { // j'utilise le hook useEffect
    axios.get(API_URL).then((response) => { // axios.get -> récupérer l'url stockée dans cont API_URL, .then -> fonction qui va permettre de 
      // récupérer les données
      setTodos(response.data); // setTodos ici récupère les données et les communique au useState pour qu'il se mette à jours
    });
  }, []);

  return (
    <div className="App">
      <TodoList todos={todos} />
    </div>
  );
}

export default App;
